cmake_minimum_required( VERSION 3.12 )
project( all )

add_subdirectory( proto )
include_directories( "${CMAKE_BINARY_DIR}/proto" )

set( LIBS yaml-cpp zmq proto jsoncpp )

add_subdirectory( src )
#add_subdirectory( tests )

