#pragma once

#include <string>
#include <cstdint>

class Publisher
{
public:
  Publisher();
  ~Publisher();

  void open( const uint64_t id );
  void close();

  void sendStartTarget( const std::string& name );
  void sendFinishTarget( const int32_t code );
  void sendCloseProject();
  void sendOutput( const std::string& message );
  void sendOutputError( const std::string& message );
  void sendPing();

private:
  void* m_socket;
};
