#include "Parser.h"
#include "Target.h"
#include "tools.h"

#include <yaml-cpp/yaml.h>
#include <iostream>
#include <stack>

namespace fs = std::filesystem;

Parser::Parser()
{
}

Parser::~Parser()
{
}

std::string Parser::getPath() const
{
  return m_path;
}

const Parser::Targets& Parser::getTargets() const
{
  return m_targets;
}

Parser::TargetList Parser::runTarget( const std::string& targetName ) const
{
  TargetList list;
  std::stack< const Target* > stack;
  std::set< std::string > depends;
  stack.push( &m_targets.at( targetName ) );
  depends.insert( targetName );
  // resolve all depends
  while( !stack.empty() )
  {
    // get target
    const auto target = stack.top();
    stack.pop();
    // get depends
    for( const auto& depend : target->getDepends() )
    {
      if( !depends.count( depend ) )
      {
        stack.push( &m_targets.at( depend ) );
        depends.insert( depend );
      }
    }
    // add to front list
    list.insert( list.begin(), target );
  }
  return std::move( list );
}

void Parser::find()
{
  fs::path current_path = fs::current_path();
  fs::path path;
  for( auto pos : current_path )
  {
    path /= pos;
    fs::path file = path / "vpro.yaml";
    if( fs::is_regular_file( file ) )
    {
      m_path = file;
    }
  }
  if( m_path.empty() )
    throw std::runtime_error( "cannot found project" );
}

void Parser::parse( const std::string& path )
{
  setPath( path );
  parse();
}

void Parser::clear()
{
  m_depends.clear();
  m_done.clear();
}

void Parser::setPath( const std::string& path )
{
  m_path = path;
  if( !fs::is_regular_file( m_path ) )
    throw std::logic_error( tools::format( "file %s no exists", m_path.c_str() ) );
}

void Parser::parse()
{
  m_targets.clear();

  YAML::Node file = YAML::LoadFile( m_path );
  if( !file.IsMap() )
    throw std::runtime_error( "invalid format - root should be map" );

  for( const auto& targetNode : file )
  {
    Target target;
    const std::string name = targetNode.first.as< std::string >();
    target.setName( name );
    target.setPath( m_path.parent_path() );
    
    const auto& data = targetNode.second;
    if( !data.IsMap() )
      throw std::runtime_error( "invalid format - target should be map" );

    if( const auto& path = data[ "path" ] )
    {
      if( !path.IsScalar() )
        throw std::runtime_error( "invalid format - target/path should be scalar" );
      auto newPath = ( m_path.parent_path() / path.as< std::string >() );
      target.setPath( std::filesystem::absolute( newPath ) );
    }

    if( const auto& env = data[ "env" ] )
      parseTargetEnvironments( target, env );

    if( const auto& let = data[ "let" ] )
      parseTargetLets( target, let );

    if( const auto& depends = data[ "depends" ] )
      parseTargetDepends( target, depends );

    if( const auto& exec = data[ "exec" ] )
      parseTargetExec( target, exec );

    m_targets.insert( { name, std::move( target ) } );
  }
}

void Parser::parseTargetDepends( Target& target, const YAML::Node& node )
{
  switch( node.Type() )
  {
  case YAML::NodeType::Scalar:
    {
      std::string name = node.as< std::string >();
      if( m_targets.find( name ) == m_targets.end() )
        throw std::runtime_error( tools::format( "depends '%s' no defined", name.c_str() ) );
      target.addDepend( name );
    }
    break;
  case YAML::NodeType::Sequence:
    for( const auto& depend : node )
    {
      if( !depend.IsScalar() )
        throw std::runtime_error( "invalid format - target/depends/sequence should be scalar" );
      std::string name = depend.as< std::string >();
      if( m_targets.find( name ) == m_targets.end() )
        throw std::runtime_error( tools::format( "depends '%s' no defined", name.c_str() ) );
      target.addDepend( name );
    }
    break;
  default:
    throw std::runtime_error( "invalid format - target/depends should be scalar/sequence" );
  }
}

void Parser::parseTargetExec( Target& target, const YAML::Node& node )
{
  switch( node.Type() )
  {
  case YAML::NodeType::Scalar:
    target.addCommand( parseCommand( node.as< std::string >() ) );
    break;
  case YAML::NodeType::Sequence:
    for( const auto& exec : node )
    {
      if( !exec.IsScalar() )
        throw std::runtime_error( "invalid format - target/exec/sequence should be scalar" );
      target.addCommand( parseCommand( exec.as< std::string >() ) );
    }
    break;
  default:
    throw std::runtime_error( "invalid format - target/exec should be scalar/sequence" );
  }
}

void Parser::parseTargetEnvironments( Target& target, const YAML::Node& node )
{
  if( !node.IsMap() )
    throw std::runtime_error( "invalid format - target/env should be map" );

  for( const auto& env : node )
  {
    const std::string name = env.first.as< std::string >();
    const auto& data = env.second;
    if( !data.IsScalar() )
      throw std::runtime_error( "invalid format - target/env/value should be scalar" );
    const std::string value = data.as< std::string >();

    target.setEnvironment( name, value );
  }
}

void Parser::parseTargetLets( Target& target, const YAML::Node& node )
{
  if( !node.IsMap() )
    throw std::runtime_error( "invalid format - target/let should be map" );

  for( const auto& let : node )
  {
    const std::string name = let.first.as< std::string >();
    const auto& data = let.second;
    if( !data.IsScalar() )
      throw std::runtime_error( "invalid format - target/let/value should be scalar" );
    const std::string value = data.as< std::string >();

    target.setLet( name, value );
  }
}

Command Parser::parseCommand( const std::string& command )
{
  Command cmd;

  std::string arg;
  arg.reserve( command.size() );
  const char* ch = command.c_str();
  const char* end = ch + command.size();

  while( ch < end )
  {
    if( ch[ 0 ] == '\\' ) 
    {
      ch++;
      if( ch < end )
      {
        arg += ch[ 0 ];
      }
    }
    else if( ch[ 0 ] == ' ' )
    {
      if( !arg.empty() )
        cmd.push_back( std::move( arg ) );
    }
    else
    {
      arg += ch[ 0 ];
    }
    ch++;
  }
  if( !arg.empty() )
    cmd.push_back( std::move( arg ) );

  return cmd;
}
