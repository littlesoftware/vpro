#include "Publisher.h"
#include "Context.h"
#include "tools.h"

#include "pubsub.pb.h"
#include <zmq.h>
#include <assert.h>

Publisher::Publisher():
  m_socket( nullptr )
{
}

Publisher::~Publisher()
{
  close();
}

void Publisher::open( const uint64_t id )
{
  if( m_socket )
    close();
  m_socket = zmq_socket( Context::get(), ZMQ_PUB );
  assert( m_socket );
  zmq_bind( m_socket, tools::getProtocolProject( id ).c_str() );
}

void Publisher::close()
{
  if( m_socket )
  {
    zmq_close( m_socket );
    m_socket = nullptr;
  }
}

void Publisher::sendStartTarget( const std::string& name )
{
  assert( m_socket );
  pubsub::Event event;
  auto startTarget = event.mutable_starttarget();
  startTarget->set_name( name );

  zmq_msg_t msg;
  std::string data;

  event.SerializeToString( &data );
  zmq_msg_init_size( &msg, data.size() );
  memcpy( zmq_msg_data( &msg ), data.c_str(), data.size() );

  zmq_msg_send( &msg, m_socket, 0 );
  zmq_msg_close( &msg );
}

void Publisher::sendFinishTarget( const int32_t code )
{
  assert( m_socket );
  pubsub::Event event;
  auto finishTarget = event.mutable_finishtarget();
  finishTarget->set_code( code );

  zmq_msg_t msg;
  std::string data;

  event.SerializeToString( &data );
  zmq_msg_init_size( &msg, data.size() );
  memcpy( zmq_msg_data( &msg ), data.c_str(), data.size() );

  zmq_msg_send( &msg, m_socket, 0 );
  zmq_msg_close( &msg );
}

void Publisher::sendCloseProject()
{
  assert( m_socket );
  pubsub::Event event;
  auto closeProject = event.mutable_closeproject();

  zmq_msg_t msg;
  std::string data;

  event.SerializeToString( &data );
  zmq_msg_init_size( &msg, data.size() );
  memcpy( zmq_msg_data( &msg ), data.c_str(), data.size() );

  zmq_msg_send( &msg, m_socket, 0 );
  zmq_msg_close( &msg );
}

void Publisher::sendOutput( const std::string& message )
{
  assert( m_socket );
  pubsub::Event event;
  auto output = event.mutable_output();
  output->set_message( message );

  zmq_msg_t msg;
  std::string data;

  event.SerializeToString( &data );
  zmq_msg_init_size( &msg, data.size() );
  memcpy( zmq_msg_data( &msg ), data.c_str(), data.size() );

  zmq_msg_send( &msg, m_socket, 0 );
  zmq_msg_close( &msg );
}

void Publisher::sendOutputError( const std::string& message )
{
  assert( m_socket );
  pubsub::Event event;
  auto outputError = event.mutable_outputerror();
  outputError->set_message( message );

  zmq_msg_t msg;
  std::string data;

  event.SerializeToString( &data );
  zmq_msg_init_size( &msg, data.size() );
  memcpy( zmq_msg_data( &msg ), data.c_str(), data.size() );

  zmq_msg_send( &msg, m_socket, 0 );
  zmq_msg_close( &msg );
}

void Publisher::sendPing()
{
  assert( m_socket );
  pubsub::Event event;
  auto ping = event.mutable_ping();

  zmq_msg_t msg;
  std::string data;

  event.SerializeToString( &data );
  zmq_msg_init_size( &msg, data.size() );
  memcpy( zmq_msg_data( &msg ), data.c_str(), data.size() );

  zmq_msg_send( &msg, m_socket, 0 );
  zmq_msg_close( &msg );
}

