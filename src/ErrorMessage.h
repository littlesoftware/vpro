#pragma once

#include <exception>
#include <string>

class ErrorMessage: public std::exception
{
public:
  ErrorMessage( const char* message ) noexcept;
  ErrorMessage( const std::string& message ) noexcept;
  virtual const char* what() const noexcept override;
private:
  std::string m_message;
};
