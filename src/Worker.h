#pragma once

#include <thread>
#include <atomic>
#include <condition_variable>
#include <mutex>
#include <queue>
#include <variant>
#include <string>
#include <memory>
#include <vector>
#include <map>

namespace subprocess
{
  class Popen;
}

class WorkerListener
{
public:
  virtual ~WorkerListener() = default;

  virtual void onOutput( const std::string& message ) = 0;
  virtual void onError( const std::string& message ) = 0;
  virtual void onFinish( const int32_t code ) = 0;
  virtual void onPing() = 0;
};

class Worker
{
public:
  using Arguments = std::vector< std::string >;
  using Environments = std::map< std::string, std::string >;
  struct Command
  {
    Arguments arguments;
    std::string path;
    Environments environments;
  };

  Worker();
  ~Worker();

  void start();
  void stop();
  void run( const Command& command );
  void kill();
  bool status() const;
  void startPing();
  void stopPing();
  void setListener( WorkerListener* listener );

private:
  enum class Type
  {
    Run,
    Kill,
    Stop
  };

  struct Event
  {
    Type type;
    std::variant< Command > data;
  };

  void worker();
  void onEvent( const Event& event );
  void processEvents();
  void processOutput();
  void processError();
  void processPing();

  std::thread m_threadWorker;
  std::mutex m_mutexWorker;
  std::condition_variable m_notifyer;
  std::queue< Event > m_events;
  std::shared_ptr< subprocess::Popen > m_process;
  bool m_running;
  std::atomic_bool m_status;
  std::atomic_bool m_ping;
  WorkerListener* m_listener;
  FILE* m_output;
  FILE* m_error;
};

