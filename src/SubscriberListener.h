#pragma once

#include <cstdint>
#include <string>

class SubscriberListener
{
public:
  virtual ~SubscriberListener() = default;

  virtual void onStartTarget( const std::string& name ) = 0;
  virtual void onFinishTarget( const int32_t code ) = 0;
  virtual void onOutput( const std::string& message ) = 0;
  virtual void onOutputError( const std::string& message ) = 0;
  virtual void onCloseProject() = 0;
  virtual void onPing() = 0;
};
