#include "Subscriber.h"
#include "SubscriberListener.h"
#include "Context.h"
#include "tools.h"
#include "pubsub.pb.h"

#include <zmq.h>

Subscriber::Subscriber():
  m_socket( nullptr ),
  m_listener( nullptr )
{
}

Subscriber::~Subscriber()
{
  close();
}

void Subscriber::connect( const uint64_t id )
{
  GOOGLE_PROTOBUF_VERIFY_VERSION;
  close();
  m_socket = zmq_socket( Context::get(), ZMQ_SUB );
  assert( m_socket );

  tools::createIpcDirectory();
  zmq_connect( m_socket, tools::getProtocolProject( id ).c_str() );
  zmq_setsockopt( m_socket, ZMQ_SUBSCRIBE, nullptr, 0 );
}

void Subscriber::close()
{
  if( m_socket )
  {
    zmq_close( m_socket );
    m_socket = nullptr;
  }
}

void Subscriber::setListener( SubscriberListener* listener )
{
  m_listener = listener;
}

void Subscriber::run()
{
  assert( m_socket );
  pubsub::Event event;
  m_running = true;
  while( m_running )
  {
    zmq_msg_t msg;
    zmq_msg_init( &msg );
    zmq_msg_recv( &msg, m_socket, 0 );

    event.ParseFromArray( zmq_msg_data( &msg ), zmq_msg_size( &msg ) );
    zmq_msg_close( &msg );

    switch( event.payload_case() )
    {
    case pubsub::Event::PayloadCase::kOutput:
      m_listener->onOutput( event.output().message() );
      break;
    case pubsub::Event::PayloadCase::kOutputError:
      m_listener->onOutputError( event.outputerror().message() );
      break;
    case pubsub::Event::PayloadCase::kStartTarget:
      m_listener->onStartTarget( event.starttarget().name() );
      break;
    case pubsub::Event::PayloadCase::kFinishTarget:
      m_listener->onFinishTarget( event.finishtarget().code() );
      break;
    case pubsub::Event::PayloadCase::kCloseProject:
      m_listener->onCloseProject();
      break;
    case pubsub::Event::PayloadCase::kPing:
      m_listener->onPing();
      break;
    default:
      break;
    }
  }
}

void Subscriber::stop()
{
  m_running = false;
}

bool Subscriber::isRunning() const
{
  return m_running;
}

