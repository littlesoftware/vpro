#pragma once

#include "ViewerListener.h"

class ViewerDebug: public ViewerListener
{
public:
  ViewerDebug() = default;
  virtual ~ViewerDebug() = default;

  virtual void onStartTarget( const std::string& name ) override;
  virtual void onFinishTarget( const int32_t code ) override;
  virtual void onOutput( const std::string& message ) override;
  virtual void onOutputError( const std::string& message ) override;
  virtual void onCloseProject() override;
  virtual void onPing() override;
  virtual void onTargetList( const uint64_t id, const TargetList& list ) override;
  virtual void onTargetDetails( const uint64_t id, const std::string& name, const TargetDetails& details ) override;
  virtual void onProjectList() override;
};
