#pragma once

#include <cstdint>
#include <atomic>

class SubscriberListener;

class Subscriber
{
public:
  Subscriber();
  ~Subscriber();

  void connect( const uint64_t id );
  void close();
  void setListener( SubscriberListener* listener );
  void run();
  void stop();
  bool isRunning() const;

private:
  void* m_socket;
  SubscriberListener* m_listener;
  std::atomic_bool m_running;
};

