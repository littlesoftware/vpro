#pragma once

#include "Target.h"

#include <string>
#include <map>
#include <mutex>
#include <unordered_set>
#include <filesystem>

namespace YAML
{
  class Node;
}

class Parser
{
public:
  using Targets = std::map< std::string, Target >;
  using TargetList = std::vector< const Target* >;

  Parser();
  ~Parser();

  std::string getPath() const;
  const Targets& getTargets() const;
  TargetList runTarget( const std::string& targetName ) const;
  void find();
  void parse( const std::string& path );
  void clear();

private:
  void setPath( const std::string& path );
  void parse();
  void parseTargetDepends( Target& target, const YAML::Node& node );
  void parseTargetExec( Target& target, const YAML::Node& node );
  void parseTargetEnvironments( Target& target, const YAML::Node& node );
  void parseTargetLets( Target& target, const YAML::Node& node );
  Command parseCommand( const std::string& command );

  Targets m_targets;
  std::filesystem::path m_path;
  // run variables
  std::unordered_set< std::string > m_depends;
  std::unordered_set< std::string > m_done;
  std::unordered_set< std::string > m_key;
  std::mutex m_runningMutex;
  Target* m_running;
};

