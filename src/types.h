#pragma once

#include <cstdint>
#include <string>
#include <vector>
#include <optional>
#include <set>
#include <list>
#include <map>

using Depends = std::set< std::string >;
using Command = std::vector< std::string >;
using Commands = std::list< Command >;
using Environments = std::map< std::string, std::string >;
using Lets = std::map< std::string, std::string >;

struct ProjectInfo
{
  uint64_t id;
  std::string path;
};

using ProjectList = std::vector< ProjectInfo >;
using TargetList = std::vector< std::string >;
using Status = std::optional< std::string >;

struct TargetDetails
{
  std::string cwd;
  Commands commands;
  Lets lets;
  Depends depends;
};
