#pragma once

#include "Publisher.h"

#include <cstddef>
#include <sstream>
#include <memory>

class Filter
{
public:
  Filter( Publisher& publisher );

  void start();
  void stop();
  void output( const std::string& message );
  void error( const std::string& message );
private:
  std::stringstream m_buffer;
  Publisher& m_publisher;
};
