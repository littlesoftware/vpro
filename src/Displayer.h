#pragma once

#include "Subscriber.h"
#include "SubscriberListener.h"
#include "ViewerListener.h"
#include "Client.h"

#include <memory>

class Displayer: public SubscriberListener
{
public:
  Displayer();
  virtual ~Displayer();

  bool setProject( const std::string& path );
  void printTargets();
  void printTargetDetails( const std::string& target );
  bool run( const std::string& target );

  void setPlainFormat();
  void setDebugFormat();
  void setJsonFormat();

  virtual void onStartTarget( const std::string& name ) override;
  virtual void onFinishTarget( const int32_t code ) override;
  virtual void onOutput( const std::string& message ) override;
  virtual void onOutputError( const std::string& message ) override;
  virtual void onCloseProject() override;
  virtual void onPing() override;

private:
  void viewer();

  Client m_client;
  Subscriber m_subscriber;
  std::shared_ptr< ViewerListener > m_viewer;
  uint64_t m_id;
  std::string m_target;
  bool m_currentTarget;
  std::atomic_bool m_viewerPingSuccess;
};
