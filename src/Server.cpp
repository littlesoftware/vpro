#include "Server.h"
#include "tools.h"
#include "Context.h"

#include <zmq.h>
#include "reqrep.pb.h"
#include "viewer.pb.h"

#include <cassert>
#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include <filesystem>

Server::Server():
  m_socket( nullptr ),
  m_listener( nullptr )
{
}

Server::~Server()
{
  close();
}

void Server::bind()
{
  GOOGLE_PROTOBUF_VERIFY_VERSION;
  m_socket = zmq_socket( Context::get(), ZMQ_REP );
  assert( m_socket );

  tools::createIpcDirectory();
  zmq_bind( m_socket, tools::getProtocolControl().c_str() );
}

void Server::run()
{
  assert( m_socket );
  reqrep::Request request;
  reqrep::Response response;
  m_request = &request;
  m_response = &response;
  while( true )
  {
    zmq_msg_t req;
    zmq_msg_init( &req );
    zmq_msg_recv( &req, m_socket, 0 );

    request.ParseFromArray( zmq_msg_data( &req ), zmq_msg_size( &req ) );
    zmq_msg_close( &req );

    try
    {
      switch( request.payload_case() )
      {
      case reqrep::Request::PayloadCase::kGetProject:
        onGetProject();
        break;
      case reqrep::Request::PayloadCase::kCloseProject:
        onCloseProject();
        break;
      case reqrep::Request::PayloadCase::kGetProjectList:
        onGetProjectList();
        break;
      case reqrep::Request::PayloadCase::kGetTargetList:
        onGetTargetList();
        break;
      case reqrep::Request::PayloadCase::kRunTarget:
        onRunTarget();
        break;
      case reqrep::Request::PayloadCase::kKillTarget:
        onKillTarget();
        break;
      case reqrep::Request::PayloadCase::kGetStatus:
        onGetStatus();
        break;
      case reqrep::Request::PayloadCase::kStartPing:
        onStartPing();
        break;
      case reqrep::Request::PayloadCase::kStopPing:
        onStopPing();
        break;
      case reqrep::Request::PayloadCase::kGetTargetDetails:
        onGetTargetDetails();
        break;
      default:
        onError( tools::format( "Unknown request type %d", request.payload_case() ) );
        break;
      }
    }
    catch( const std::exception& err )
    {
      onError( err.what() );
    }
    catch( ... )
    {
      onError( "unknown error" );
    }

    // response
    zmq_msg_t resp;
    std::string data;
    response.SerializeToString( &data );
    zmq_msg_init_size( &resp, data.size() );
    memcpy( zmq_msg_data( &resp ), data.c_str(), data.size() );

    zmq_msg_send( &resp, m_socket, 0 );
    zmq_msg_close( &resp );
  }
}

void Server::close()
{
  if( m_socket )
  {
    zmq_close( m_socket );
    m_socket = nullptr;
  }
}

void Server::setListener( ServerListener* listener )
{
  m_listener = listener;
}

void Server::onSuccess()
{
  m_response->mutable_success();
}

void Server::onError( const std::string& message )
{
  auto error = m_response->mutable_error();
  error->set_message( message );
}

void Server::onGetProject()
{
  const auto& path = m_request->getproject().path();
  const auto info = m_listener->onGetProject( path );

  auto project = m_response->mutable_project();
  project->set_id( info.id );
  project->set_path( info.path );
}

void Server::onGetProjectList()
{
  const auto list = m_listener->onGetProjectList();

  auto projectList = m_response->mutable_projectlist();
  projectList->clear_projects();
  for( const auto& info : list )
  {
    auto pro = projectList->add_projects();
    pro->set_id( info.id );
    pro->set_path( info.path );
  }
}

void Server::onCloseProject()
{
  const auto& id = m_request->closeproject().id();
  m_listener->onCloseProject( id );
  onSuccess();
}

void Server::onGetTargetList()
{
  const auto& id = m_request->gettargetlist().id();
  const auto list = m_listener->onGetTargetList( id );

  auto targetList = m_response->mutable_targetlist();
  targetList->clear_targets();
  for( const auto& name : list )
  {
    auto target = targetList->add_targets();
    target->assign( name );
  }
}

void Server::onRunTarget()
{
  const auto& id = m_request->runtarget().id();
  const auto& target = m_request->runtarget().target();
  m_listener->onRunTarget( id, target );
  onSuccess();
}

void Server::onKillTarget() 
{
  const auto& id = m_request->killtarget().id();
  m_listener->onKillTarget( id );
}

void Server::onGetStatus()
{
  const auto& id = m_request->getstatus().id();
  const auto st = m_listener->onGetStatus( id );

  auto status = m_response->mutable_status();
  if( st )
    status->set_target( st.value() );
  else
    status->clear_target();
}

void Server::onStartPing()
{
  const auto& id = m_request->startping().id();
  m_listener->onStartPing( id );
  onSuccess();
}

void Server::onStopPing()
{
  const auto& id = m_request->stopping().id();
  m_listener->onStopPing( id );
  onSuccess();
}

void Server::onGetTargetDetails()
{
  const auto& id = m_request->gettargetdetails().id();
  const auto& target = m_request->gettargetdetails().target();
  const auto details = m_listener->onGetTargetDetails( id, target );

  auto targetDetails = m_response->mutable_targetdetails();
  // cwd
  targetDetails->set_cwd( details.cwd );
  // depends
  targetDetails->clear_depends();
  for( const auto& name : details.depends )
    targetDetails->add_depends( name );
  // lets
  targetDetails->clear_lets();
  for( const auto& [ name, value ] : details.lets )
  {
    auto l = targetDetails->add_lets();
    l->set_name( name );
    l->set_value( value );
  }
  // commands
  targetDetails->clear_commands();
  for( const auto& command : details.commands )
  {
    auto cmd = targetDetails->add_commands();
    for( const auto& arg : command )
      cmd->add_args( arg );
  }
}

