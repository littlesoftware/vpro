#pragma once

#include <string>
#include <vector>
#include <cstdint>

namespace tools
{

std::string format( const char* fmt, ... );
std::vector< std::string > split( const std::string& str, char delimiter, bool empty = true );
void createIpcDirectory();
const std::string& getProtocolControl();
std::string getProtocolProject( const uint64_t id );
std::string findProject();
std::string findProject( const std::string& path );
bool isServerRunned();
bool lockServer( const bool lock = true );
void initKbhit( FILE* file );
int kbhit( FILE* file );

} // namespace tools
