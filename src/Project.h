#pragma once

#include "Parser.h"
#include "Worker.h"
#include "Publisher.h"
#include "Filter.h"

#include <stdint.h>
#include <string>
#include <queue>
#include <filesystem>

class Project: public WorkerListener
{
public:
  Project();
  ~Project();

  uint64_t open( const std::string& path );
  void close();
  Worker& worker();
  Publisher& publisher();
  Parser& parser();
  uint64_t getId() const;
  void runTarget( const std::string& targetName );

  virtual void onOutput( const std::string& message ) override;
  virtual void onError( const std::string& message ) override;
  virtual void onFinish( const int32_t code ) override;
  virtual void onPing() override;

private:
  void invoke();
  bool invokeCommand();
  bool invokeTarget();
  bool pathIsCorrect( const std::string& path ) const;
  void onStartTarget( const std::string& name );
  void onEndTarget( const int32_t code );
  static Environments mergeEnvironments( const std::list< Environments >& envs );
  static Environments getSystemEnvironments();

  uint64_t m_id;
  Parser m_parser;
  Worker m_worker;
  Publisher m_publisher;
  const Target* m_currentTarget;
  std::queue< const Target* > m_targets;
  std::queue< Command > m_commands;
  std::string m_path;
  std::filesystem::file_time_type m_lastWriteTime;
  Filter m_filter;
};
