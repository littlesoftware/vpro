#pragma once

#include "types.h"

#include <string>
#include <vector>
#include <list>
#include <set>
#include <optional>
#include <map>

class Target
{
public:
  Target();
  ~Target();

  void setName( const std::string& name );
  const std::string& getName() const;
  void addDepend( const std::string& name );
  const Depends& getDepends() const;
  void setPath( const std::string& path );
  const std::string& getPath() const;
  void addCommand( const Command& command );
  const Commands& getCommands() const;
  void setEnvironment( const std::string& name, const std::string& value );
  const Environments& getEnvironments() const;
  void setLet( const std::string& name, const std::string& value );
  const Lets& getLets() const;

private:
  std::string m_name;
  Depends m_depends;
  std::string m_path;
  Commands m_commands;
  Environments m_environments;
  Lets m_lets;
};
