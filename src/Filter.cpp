#include "Filter.h"

#include <iostream>
#include <assert.h>

Filter::Filter( Publisher& publisher ):
  m_publisher( publisher )
{
}

void Filter::start()
{
}

void Filter::stop()
{
}

void Filter::output( const std::string& message )
{
  m_publisher.sendOutput( message );
}

void Filter::error( const std::string& message )
{
  m_publisher.sendOutputError( message );
}
