#include "Client.h"
#include "tools.h"
#include "Context.h"
#include "ErrorMessage.h"

#include "reqrep.pb.h"
#include <zmq.h>

#include <cassert>
#include <cstring>
#include <iostream>

Client::Client():
  m_socket( nullptr )
{
}

Client::~Client()
{
  close();
}

void Client::connect()
{
  GOOGLE_PROTOBUF_VERIFY_VERSION;
  close();
  m_socket = zmq_socket( Context::get(), ZMQ_REQ );
  assert( m_socket );

  tools::createIpcDirectory();
  zmq_connect( m_socket, tools::getProtocolControl().c_str() );
}

inline void require( const reqrep::Request& request, reqrep::Response& response, const reqrep::Response::PayloadCase type, void* socket )
{
  std::string data;
  request.SerializeToString( &data );
  // send
  zmq_msg_t req;
  zmq_msg_init_size( &req, data.size() );
  memcpy( zmq_msg_data( &req ), data.c_str(), data.size() );

  zmq_msg_send( &req, socket, 0 );
  zmq_msg_close( &req );
  // recv
  zmq_msg_t resp;
  zmq_msg_init( &resp );
  zmq_msg_recv( &resp, socket, 0 );

  response.ParseFromArray( zmq_msg_data( &resp ), zmq_msg_size( &resp ) );
  zmq_msg_close( &resp );

  if( type == response.payload_case() )
    return;
  switch( response.payload_case() )
  {
  case reqrep::Response::PayloadCase::kError:
    throw ErrorMessage( response.error().message() );
    break;
  default:
    throw std::logic_error( tools::format( "Unexpected response %d", response.payload_case() ) );
    break;
  }
}

uint64_t Client::getProject( const std::string& path )
{
  reqrep::Request request;
  reqrep::Response response;
  // make request
  auto getProject = request.mutable_getproject();
  getProject->set_path( path );

  require( request, response, reqrep::Response::PayloadCase::kProject, m_socket );
  return response.project().id();
}

void Client::closeProject( const uint64_t id )
{
  reqrep::Request request;
  reqrep::Response response;
  // make request
  auto closeProject = request.mutable_closeproject();
  closeProject->set_id( id );

  require( request, response, reqrep::Response::PayloadCase::kSuccess, m_socket );
}

ProjectList Client::getProjectList()
{
  reqrep::Request request;
  reqrep::Response response;
  // make request
  auto getProjectLlist = request.mutable_getprojectlist();

  require( request, response, reqrep::Response::PayloadCase::kProjectList, m_socket );
  ProjectList list;
  const auto& p = response.projectlist();
  list.reserve( p.projects_size() );
  for( const auto& proj : p.projects() )
  {
    list.push_back( {
      proj.id(),
      proj.path()
    } );
  }
  return std::move( list );
}

TargetList Client::getTargetList( const uint64_t id )
{
  reqrep::Request request;
  reqrep::Response response;
  // make request
  auto getTargetLlist = request.mutable_gettargetlist();
  getTargetLlist->set_id( id );

  require( request, response, reqrep::Response::PayloadCase::kTargetList, m_socket );
  TargetList list;
  const auto& p = response.targetlist();
  list.reserve( p.targets_size() );
  for( const auto& name : p.targets() )
  {
    list.push_back( name );
  }
  return std::move( list );
}

void Client::runTarget( const uint64_t id, const std::string& name )
{
  reqrep::Request request;
  reqrep::Response response;
  // make request
  auto runTarget = request.mutable_runtarget();
  runTarget->set_id( id );
  runTarget->set_target( name );

  require( request, response, reqrep::Response::PayloadCase::kSuccess, m_socket );
}

void Client::killTarget( const uint64_t id )
{
  reqrep::Request request;
  reqrep::Response response;
  // make request
  auto killTarget = request.mutable_killtarget();
  killTarget->set_id( id );

  require( request, response, reqrep::Response::PayloadCase::kSuccess, m_socket );
}

Status Client::getStatus( const uint64_t id )
{
  reqrep::Request request;
  reqrep::Response response;
  // make request
  auto getStatus = request.mutable_getstatus();
  getStatus->set_id( id );

  require( request, response, reqrep::Response::PayloadCase::kStatus, m_socket );
  std::optional< std::string > result;
  const auto& p = response.status();
  if( p.has_target() )
    result = p.target();
  return result;
}

void Client::startPing( const uint64_t id )
{
  reqrep::Request request;
  reqrep::Response response;
  // make request
  auto startPing = request.mutable_startping();
  startPing->set_id( id );

  require( request, response, reqrep::Response::PayloadCase::kSuccess, m_socket );
}

void Client::stopPing( const uint64_t id )
{
  reqrep::Request request;
  reqrep::Response response;
  // make request
  auto stopPing = request.mutable_stopping();
  stopPing->set_id( id );

  require( request, response, reqrep::Response::PayloadCase::kSuccess, m_socket );
}

TargetDetails Client::getTargetDetails( const uint64_t id, const std::string& name )
{
  reqrep::Request request;
  reqrep::Response response;
  // make request
  auto getTargetDetails = request.mutable_gettargetdetails();
  getTargetDetails->set_id( id );
  getTargetDetails->set_target( name );

  require( request, response, reqrep::Response::PayloadCase::kTargetDetails, m_socket );
  TargetDetails result;
  const auto& targetDetails = response.targetdetails();
  result.cwd = targetDetails.cwd();
  for( const auto& depend : targetDetails.depends() )
    result.depends.insert( depend );
  for( const auto& cmd : targetDetails.commands() )
    result.commands.push_back( Command( cmd.args().cbegin(), cmd.args().cend() ) );
  for( const auto& let : targetDetails.lets() )
    result.lets.insert( { let.name(), let.value() } );
  return result;
}

void Client::close()
{
  if( m_socket )
  {
    zmq_close( m_socket );
    m_socket = nullptr;
  }
}
