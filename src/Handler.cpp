#include "Handler.h"
#include "Worker.h"
#include "ErrorMessage.h"
#include "Displayer.h"
#include "tools.h"

#include <iostream>
#include <fstream>
#include <csignal>
#include <ostream>
#include <inttypes.h>
#include <getopt.h>

Handler::Handler():
  m_action( Action::Unknown )
{
}

Handler::~Handler()
{
}

int Handler::execute( int argc, char** argv )
{
  Handler& handler = instance();
  try
  {
    handler.parseOptions( argc, argv );

    switch( handler.getAction() )
    {
    case Action::Target:
      signal( SIGINT, Handler::onSignal );
    case Action::TargetDetails:
    case Action::List:
      handler.performClient();
      break;
    case Action::Path:
      handler.printPath();
      break;
    case Action::Version:
      handler.printVersion();
      break;
    case Action::Help:
      handler.printHelp();
      break;
    default:
      std::cerr << "unknown action" << std::endl;
      return 1;
    }
  }
  catch( const std::exception& err )
  {
    std::cerr << "error: " << err.what() << std::endl;
    return 2;
  }
  catch( ... )
  {
    std::cerr << "error: unknown error" << std::endl;
    return 3;
  }
  return 0;
}

Handler& Handler::instance()
{
  static Handler obj;
  return obj;
}

void Handler::parseOptions( int argc, char** argv )
{
  const option long_options[] = {
    { "path", no_argument, nullptr, 'P' },
    { "list", no_argument, nullptr, 'l' },
    { "format", required_argument, nullptr, 'f' },
    { "folder", required_argument, nullptr, 'd' },
    { "help", no_argument, nullptr, 'H' },
    { "version", no_argument, nullptr, 'V' },
    { "projects", no_argument, nullptr, 'p' },
    { "details", no_argument, nullptr, 'D' },
    { nullptr, 0, nullptr, 0 }
  };
  const char short_options[] = "HVPDlpf:d:";
  int name;
  int argind;
  m_action = Action::Unknown;

  while( ( name = getopt_long(argc, argv, short_options, long_options, nullptr ) ) != -1 )
  {
    switch(name)
    {
    case 'P':
      m_action = Action::Path;
      break;
    case 'H':
      m_action = Action::Help;
      break;
    case 'V':
      m_action = Action::Version;
      break;
    case 'D':
      m_action = Action::TargetDetails;
      break;
    case 'p':
      m_action = Action::Projects;
      break;
    case 'l':
      m_action = Action::List;
      break;
    case 'd':
      m_folder = optarg;
      break;
    case 'f':
      m_format = optarg;
      break;
    }
  }

  argind = optind;
  for( ; argind < argc; argind++ )
  {
    m_targets.push_back( argv[argind] );
  }

  if( m_action == Action::Unknown )
  {
    m_action = m_targets.empty() ? Action::List : Action::Target;
  }
}

Handler::Action Handler::getAction() const
{
  return m_action;
}

void Handler::makeServer()
{
  if( tools::lockServer() )
  {
    // in not parent process we run server
    if( fork() == 0 )
    {
      Manager manager;
      manager.runServer();
      exit( 0 );
    }
  }
}

void Handler::printPath()
{
  const auto path = tools::findProject();
  std::cout << path << std::endl;
}

void Handler::onTarget()
{
}

void Handler::onList()
{
}

void Handler::performClient()
{
  const auto path = m_folder.empty() ?
                    tools::findProject() :
                    tools::findProject( m_folder );

  makeServer();

  Displayer displayer;
  displayer.setProject( path );

  if( m_format == "json" )
  {
    displayer.setJsonFormat();
  }
  else if( m_format == "debug" )
  {
    displayer.setDebugFormat();
  }
  
  switch( m_action )
  {
  case Action::Target:
    for( const auto& target : m_targets )
    {
      displayer.run( target );
    }
    break;
  case Action::Projects:
    // TODO
    break;
  case Action::List:
    displayer.printTargets();
    break;
  case Action::TargetDetails:
    for( const auto& target : m_targets )
      displayer.printTargetDetails( target );
    break;
  default:
    break;
  }
}

void Handler::printHelp()
{
  std::cout <<
    "Usage: vpro [OPTION]... [TARGET]..." << std::endl <<
    "Perform TARGET(s) in the project." << std::endl <<
    std::endl <<
    "With no TARGET, print all target list." << std::endl <<
    "With TARGET, default action is perform this targets" << std::endl <<
    std::endl <<
    "  -H, --help             display this help and exit" << std::endl <<
    "  -V, --version          output version information and exit" << std::endl <<
    "  -P, --path             print found project file" << std::endl <<
    "  -l, --list             print list of targets" << std::endl <<
    "  -p, --projects         print list of projects" << std::endl <<
    "  -f, --format=[FMT]     output format FMT = plain, json, debug" << std::endl <<
    "  -d, --folder=[DIR]     find project from DIR folder" << std::endl <<
    std::endl <<
    "Examples:" << std::endl <<
    "  vpro         Print target list." << std::endl <<
    "  vpro build   Perform target by name 'build'." << std::endl;
}

void Handler::printVersion()
{
  std::cout <<
    "vpro 3.0" << std::endl <<
    "Copyright (C) 2023 Free Software Foundation, Inc." << std::endl <<
    "License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>." << std::endl <<
    "This is free software: you are free to change and redistribute it." << std::endl <<
    "There is NO WARRANTY, to the extent permitted by law." << std::endl <<
    std::endl <<
    "Written by Nikolaev Anton." << std::endl;
}

void Handler::onSignal( int )
{
  //instance().kill();
}

