#include "ViewerJson.h"

#include <json/json.h>
#include <iostream>

void ViewerJson::onStartTarget( const std::string& name )
{
  Json::Value root;
  Json::FastWriter writer;

  root[ "type" ] = "start";
  root[ "name" ] = name;

  std::cout << writer.write( root );
}

void ViewerJson::onFinishTarget( const int32_t code )
{
  Json::Value root;
  Json::FastWriter writer;

  root[ "type" ] = "finish";
  root[ "code" ] = code;

  std::cout << writer.write( root );
}

void ViewerJson::onOutput( const std::string& message )
{
  Json::Value root;
  Json::FastWriter writer;

  root[ "type" ] = "stdout";
  root[ "message" ] = message;

  std::cout << writer.write( root );
}

void ViewerJson::onOutputError( const std::string& message )
{
  Json::Value root;
  Json::FastWriter writer;

  root[ "type" ] = "stderr";
  root[ "message" ] = message;

  std::cout << writer.write( root );
}

void ViewerJson::onCloseProject()
{
  Json::Value root;
  Json::FastWriter writer;

  root[ "type" ] = "close";

  std::cout << writer.write( root );
}

void ViewerJson::onPing()
{
  Json::Value root;
  Json::FastWriter writer;

  root[ "type" ] = "ping";

  std::cout << writer.write( root );
}

void ViewerJson::onTargetList( const uint64_t id, const TargetList& list )
{
  Json::Value root;
  Json::FastWriter writer;

  root[ "type" ] = "targets";
  root[ "id" ] = id;
  Json::Value& jsonList = root[ "list" ];
  jsonList = Json::arrayValue;
  for( const auto& name : list )
  {
    jsonList.append( name );
  }

  std::cout << writer.write( root );
}

void ViewerJson::onTargetDetails( const uint64_t id, const std::string& name, const TargetDetails& details )
{
  Json::Value root;
  Json::FastWriter writer;

  root[ "type" ] = "target details";
  root[ "id" ] = id;
  root[ "name" ] = name;
  root[ "cwd" ] = details.cwd;
  Json::Value& jsonDepends = root[ "depends" ];
  jsonDepends = Json::arrayValue;
  for( const auto& depend : details.depends )
  {
    jsonDepends.append( depend );
  }

  Json::Value& jsonLets = root[ "lets" ];
  jsonLets = Json::objectValue;
  for( const auto& let : details.lets )
  {
    jsonLets[ let.first ] = let.second;
  }

  Json::Value& jsonCommands = root[ "commands" ];
  jsonCommands = Json::arrayValue;
  for( const auto& command : details.commands )
  {
    Json::Value jsonCommand;
    jsonCommand = Json::arrayValue;
    for( const auto& arg : command )
      jsonCommand.append( arg );
    jsonCommands.append( std::move( jsonCommand ) );
  }

  std::cout << writer.write( root );
}

void ViewerJson::onProjectList()
{
  Json::Value root;
  Json::FastWriter writer;

  root[ "type" ] = "projects";

  std::cout << writer.write( root );
}
