#pragma once

#include "ServerListener.h"

#include <string>
#include <map>
#include <cstdint>
#include <memory>
#include <queue>

namespace reqrep
{
  class Request;
  class Response;
} // namespace reqrep

class Server
{
public:
  Server();
  ~Server();

  void bind();
  void run();
  void close();
  void setListener( ServerListener* listener );

  void onSuccess();
  void onError( const std::string& message );
  void onGetProject();
  void onGetProjectList();
  void onCloseProject();
  void onGetTargetList();
  void onRunTarget();
  void onKillTarget();
  void onGetStatus();
  void onStartPing();
  void onStopPing();
  void onGetTargetDetails();

private:
  void* m_socket;
  reqrep::Request* m_request;
  reqrep::Response* m_response;
  ServerListener* m_listener;
};
