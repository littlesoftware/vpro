#include "ErrorMessage.h"

ErrorMessage::ErrorMessage( const char* message ) noexcept:
  m_message( message )
{
}

ErrorMessage::ErrorMessage( const std::string& message ) noexcept:
  m_message( message )
{
}

const char* ErrorMessage::what() const noexcept
{
  return m_message.c_str();
}
