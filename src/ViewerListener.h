#pragma once

#include "SubscriberListener.h"
#include "types.h"

class ViewerListener: public SubscriberListener
{
public:
  virtual ~ViewerListener() = default;

  virtual void onTargetList( const uint64_t id, const TargetList& list ) = 0;
  virtual void onTargetDetails( const uint64_t id, const std::string& name, const TargetDetails& details ) = 0;
  virtual void onProjectList() = 0;
};
