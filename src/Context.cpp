#include "Context.h"
#include <cassert>
#include <zmq.h>

Context::Context()
{
  m_context = zmq_ctx_new();
  assert( m_context );
}

Context::~Context()
{
  zmq_ctx_destroy( m_context );
}

void* Context::get()
{
  static Context obj;
  return obj.m_context;
}

