#include "ViewerDebug.h"

#include <iostream>

void ViewerDebug::onStartTarget( const std::string& name )
{
  std::cout << "\033[1;34m" << "Start target " << name << "\033[0m" << std::endl;
}

void ViewerDebug::onFinishTarget( const int32_t code )
{
  std::cout << "\033[1;34m" << "Finish target " << code << "\033[0m" << std::endl;
}

void ViewerDebug::onOutput( const std::string& message )
{
  std::cout << "\033[1;32m" << "Output message" << "\033[0m" << std::endl;
  std::cout << message << std::endl;
}

void ViewerDebug::onOutputError( const std::string& message )
{
  std::cout << "\033[1;31m" << "Error message" << "\033[0m" << std::endl;
  std::cout << message << std::endl;
}

void ViewerDebug::onCloseProject()
{
  std::cout << "\033[1;34m" << "Close project" << "\033[0m" << std::endl;
}

void ViewerDebug::onPing()
{
  std::cout << "\033[1;33m" << "Ping" << "\033[0m" << std::endl;
}

void ViewerDebug::onTargetList( const uint64_t id, const TargetList& list )
{
  std::cout << "\033[1;32m" << "Target list of project " << id << "\033[0m" << std::endl;
  for( const auto& name : list )
  {
    std::cout << name << std::endl;
  }
}

void ViewerDebug::onTargetDetails( const uint64_t id, const std::string& name, const TargetDetails& details )
{
  std::cout << "\033[1;32m" << "Target " << name << " details" << std::endl;
  std::cout << "\033[1;34m" << "Project: " << "\033[1;33m"  << id << std::endl;
  std::cout << "\033[1;34m" << "Cwd: " << "\033[1;33m"  << details.cwd << std::endl;
  std::cout << "\033[1;34m" << "Depends: " << "\033[1;33m" << std::endl;
  for( const auto& depend : details.depends )
  {
    std::cout << "  -> " << depend << std::endl;
  }
  std::cout << "\033[1;34m" << "Lets: " << "\033[1;33m" << std::endl;
  for( const auto& let : details.lets )
  {
    std::cout << "  -> " << let.first << " = " << let.second << std::endl;
  }
  std::cout << "\033[1;34m" << "Commands: " << "\033[1;33m" << std::endl;
  for( const auto& command : details.commands )
  {
    std::cout << "  -> ";
    for( const auto& arg : command )
      std::cout << arg << " ";
    std::cout << std::endl;
  }
  std::cout << "\033[0m";
}

void ViewerDebug::onProjectList()
{
  std::cout << "\033[1;32m" << "Project list" << "\033[0m" << std::endl;
}
