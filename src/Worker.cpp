#include "Worker.h"
#include "tools.h"

#include <cpp-subprocess/subprocess.hpp>
#include <iostream>
#include <memory>
#include <algorithm>

Worker::Worker():
  m_status( false ),
  m_listener( nullptr ),
  m_ping( false )
{
}

Worker::~Worker()
{
  stop();
}

void Worker::start()
{
  {
    std::lock_guard< std::mutex > locker( m_mutexWorker );
    while( !m_events.empty() )
      m_events.pop();
  }
  m_threadWorker = std::thread( &Worker::worker, this );
}

void Worker::stop()
{
  {
    std::lock_guard< std::mutex > locker( m_mutexWorker );
    m_events.push( { Type::Stop } );
  }
  m_notifyer.notify_one();

  if( m_threadWorker.joinable() )
    m_threadWorker.join();
}

void Worker::run( const Command& command )
{
  if( std::this_thread::get_id() != m_threadWorker.get_id() ){
    std::lock_guard< std::mutex > locker( m_mutexWorker );
    m_events.push( { Type::Run, command } );
  }
  else
  {
    m_events.push( { Type::Run, command } );
  }
  m_notifyer.notify_one();
}

void Worker::kill()
{
  {
    std::lock_guard< std::mutex > locker( m_mutexWorker );
    std::cout << "push kill event" << std::endl;
    m_events.push( { Type::Kill } );
  }
  m_notifyer.notify_one();
}

bool Worker::status() const
{
  return m_status;
}

void Worker::startPing()
{
  m_ping = true;
}

void Worker::stopPing()
{
  m_ping = false;
}

void Worker::setListener( WorkerListener* listener )
{
  m_listener = listener;
}

void Worker::worker()
{
  m_running = true;
  while( m_running )
  {
    std::unique_lock< std::mutex > locker( m_mutexWorker );
    m_notifyer.wait_for( locker, std::chrono::milliseconds( 10 ), [&]{
      return !m_events.empty();
    } );
    processEvents();
    processPing();
    if( m_process )
    {
      processOutput();
      processError();
      auto poll = m_process->poll();
      if( poll != -1 )
      {
        auto code = m_process->retcode();
        m_process.reset();
        m_status = false;
        m_listener->onFinish( code );
      }
    }
  }
}

void Worker::onEvent( const Event& event )
{
  switch( event.type )
  {
  case Type::Run:
    if( !m_process )
    {
      m_status = true;
      const auto& cmd = std::get< Command >( event.data );
      m_process = std::make_shared< subprocess::Popen >(
        cmd.arguments,
        subprocess::output{ subprocess::PIPE },
        subprocess::error{ subprocess::PIPE },
        subprocess::input{ subprocess::PIPE },
        subprocess::environment{ cmd.environments },
        subprocess::cwd{ cmd.path } );
      m_output = m_process->output();
      m_error = m_process->error();
      tools::initKbhit( m_output );
      tools::initKbhit( m_error );
    }
    // TODO: else error. process is yet runned
    break;
  case Type::Stop:
    if( !m_process )
      m_running = false;
    // TODO: else error. need to kill
    break;
  case Type::Kill:
    if( m_process )
    {
      std::cout << "sending kill" << std::endl;
      m_process->kill( SIGKILL );
      m_process->wait();
      std::cout << "kill ok" << std::endl;
    }
    // TODO: else error. no process
    break;
  }
}

void Worker::processEvents()
{
  while( !m_events.empty() )
  {
    onEvent( m_events.front() );
    m_events.pop();
  }
}

void Worker::processOutput()
{
  if( !m_process )
    return;
  char buffer[1000];
  int available = tools::kbhit( m_output );
  while( available > 0 )
  {
    const auto block = std::min< uint64_t >( available, 1000 );
    fread( buffer, block, 1, m_output );
    m_listener->onOutput( std::string( buffer, block ) );
    available -= block;
  }
}

void Worker::processError()
{
  if( !m_process )
    return;
  char buffer[1000];
  int available = tools::kbhit( m_error );
  while( available > 0 )
  {
    const auto block = std::min< uint64_t >( available, 1000 );
    fread( buffer, block, 1, m_error );
    m_listener->onError( std::string( buffer, block ) );
    available -= block;
  }
}

void Worker::processPing()
{
  if( m_ping )
    m_listener->onPing();
}

