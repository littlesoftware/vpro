#include "ViewerPlain.h"

#include <iostream>

void ViewerPlain::onStartTarget( const std::string& name )
{
}

void ViewerPlain::onFinishTarget( const int32_t code )
{
}

void ViewerPlain::onOutput( const std::string& message )
{
  std::cout << message;
}

void ViewerPlain::onOutputError( const std::string& message )
{
  std::cout << message;
}

void ViewerPlain::onCloseProject()
{
}

void ViewerPlain::onPing()
{
}

void ViewerPlain::onTargetList( const uint64_t, const TargetList& list )
{
  for( const auto& name : list )
  {
    std::cout << name << std::endl;
  }
}

void ViewerPlain::onTargetDetails( const uint64_t id, const std::string& name, const TargetDetails& details )
{
  std::cout << "Project: " << id << std::endl;
  std::cout << "Target: " << name << std::endl;
  std::cout << "Cwd: " << details.cwd << std::endl;
  std::cout << "Depends:";
  for( const auto& depend : details.depends )
    std::cout << " " <<  depend;
  std::cout << std::endl;

  std::cout << "Lets:" << std::endl;
  for( const auto& let : details.lets )
    std::cout << "  " << let.first << " = " << let.second << std::endl;

  std::cout << "Commands:" << std::endl;
  for( const auto& command : details.commands )
  {
    std::cout << "  ";
    for( const auto& arg : command )
      std::cout << " " << arg;
    std::cout << std::endl;
  }
}

void ViewerPlain::onProjectList()
{
}
