#pragma once

#include "Parser.h"
#include "Client.h"
#include "Manager.h"
#include "types.h"

#include <string>
#include <vector>

class Handler
{
public:
  enum class Action
  {
    Unknown,
    Target,
    Path,
    Version,
    List,
    Help,
    Projects,
    TargetDetails
  };

  Handler();
  ~Handler();

  static int execute( int argc, char** argv );
  static Handler& instance();

  void parseOptions( int argc, char** argv );
  Action getAction() const;

  void makeServer();
  void printPath();
  void onTarget();
  void onList();
  void performClient();
  void printHelp();
  void printVersion();

private:
  static void onSignal( int );

  Action m_action;
  TargetList m_targets;
  std::string m_folder;
  std::string m_format;
};
