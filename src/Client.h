#pragma once

#include "types.h"

#include <string>
#include <cstdint>
#include <optional>

class Client
{
public:
  Client();
  ~Client();

  void connect();

  uint64_t getProject( const std::string& path );
  void closeProject( const uint64_t id );
  ProjectList getProjectList();
  TargetList getTargetList( const uint64_t id );
  void runTarget( const uint64_t id, const std::string& name );
  void killTarget( const uint64_t id );
  Status getStatus( const uint64_t id );
  void startPing( const uint64_t id );
  void stopPing( const uint64_t id );
  TargetDetails getTargetDetails( const uint64_t id, const std::string& name );

  void close();

private:
  void* m_socket;
};
