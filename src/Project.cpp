#include "Project.h"

#include <iostream>
#include <queue>
#include <cstring>
#include <assert.h>

std::queue< uint64_t > g_freeIds;
uint64_t g_lastId = 0;
extern "C" char** environ;

Project::Project():
  m_id( 0 ),
  m_currentTarget( nullptr ),
  m_filter( m_publisher )
{
}

Project::~Project()
{
  close();
}

uint64_t Project::open( const std::string& path )
{
  assert( m_id == 0 );
  if( g_freeIds.empty() )
  {
    m_id = ++g_lastId;
  }
  else
  {
    m_id = g_freeIds.front();
    g_freeIds.pop();
  }
  m_parser.parse( path );
  m_lastWriteTime = std::filesystem::last_write_time( path );
  m_publisher.open( m_id );
  m_worker.setListener( this );
  m_worker.start();

  return m_id;
}

void Project::close()
{
  if( m_id != 0 )
  {
    m_worker.kill();
    m_worker.stop();
    m_publisher.sendCloseProject();
    m_publisher.close();
    g_freeIds.push( m_id );
    m_id = 0;
  }
}

Worker& Project::worker()
{
  return m_worker;
}

Publisher& Project::publisher()
{
  return m_publisher;
}

Parser& Project::parser()
{
  return m_parser;
}

uint64_t Project::getId() const
{
  return m_id;
}

void Project::runTarget( const std::string& targetName )
{
  // check last write time
  const auto lastWriteTime = std::filesystem::last_write_time( m_parser.getPath() );
  if( lastWriteTime != m_lastWriteTime )
  {
    m_lastWriteTime = lastWriteTime;
    m_parser.parse( m_parser.getPath() );
  }

  for( const auto& target : m_parser.runTarget( targetName ) )
  {
    m_targets.push( target );
  }
  invoke();
}

void Project::onOutput( const std::string& message )
{
  m_filter.output( message );
}

void Project::onError( const std::string& message )
{
  m_filter.error( message );
}

void Project::onFinish( const int32_t code )
{
  onEndTarget( code );
}

void Project::onPing()
{
  m_publisher.sendPing();
}

void Project::invoke()
{
  // is running
  if( m_worker.status() )
    return;
  while( true )
  {
    if( invokeCommand() )
      break;
    if( m_currentTarget )
      m_publisher.sendFinishTarget( 0 );
    if( !invokeTarget() )
    {
      m_currentTarget = nullptr;
      break;
    }
  }
}

bool Project::invokeCommand()
{
  if( m_commands.empty() )
    return false;
  const auto& cmd = m_commands.front();
  m_worker.run( {
      cmd,
      m_currentTarget->getPath(),
      mergeEnvironments( {
          m_currentTarget->getEnvironments(),
          getSystemEnvironments()
      } )
  } );
  m_commands.pop();
  return true;
}

bool Project::invokeTarget()
{
  if( m_targets.empty() )
    return false;
  const auto target = m_targets.front();
  m_currentTarget = target;
  onStartTarget( target->getName() );
  if( !pathIsCorrect( target->getPath() ) )
  {
    onEndTarget( -1 );
    return false;
  }
  // TODO: check and load filter plugin
  for( const auto& cmd : target->getCommands() )
    m_commands.push( cmd );
  m_targets.pop();
  return true;
}

bool Project::pathIsCorrect( const std::string& path ) const
{
  return std::filesystem::is_directory( path );
}

void Project::onStartTarget( const std::string& name )
{
  m_publisher.sendStartTarget( name );
  m_filter.start();
}

void Project::onEndTarget( const int32_t code )
{
  m_filter.stop();
  if( code )
  {
    m_commands = {};
    m_targets = {};
    m_currentTarget = nullptr;
    m_publisher.sendFinishTarget( code );
  }
  else
  {
    invoke();
  }
}

Environments Project::mergeEnvironments( const std::list< Environments >& envs )
{
  Environments result;
  for( const auto& e : envs )
  {
    result.insert( e.begin(), e.end() );
  }
  return result;
}

Environments Project::getSystemEnvironments()
{
  Environments result;
  for( int i = 0; environ[i] != nullptr; i++ )
  {
    const char* env = environ[i];
    const char* idx = std::strchr( env, '=' );
    std::string name;
    std::string value;
    if( idx == nullptr )
    {
      name = env;
    }
    else
    {
      name = std::string( env, idx - env );
      value = std::string( idx + 1 );
    }
    result.insert( { name, value } );
  }
  return result;
}
