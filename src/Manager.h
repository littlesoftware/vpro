#pragma once

#include "Project.h"
#include "Server.h"
#include "ServerListener.h"

#include <memory>
#include <map>
#include <queue>

class Manager: public ServerListener
{
public:
  Manager();
  ~Manager();

  void runServer();

  virtual ProjectInfo onGetProject( const std::string& path ) override;
  virtual ProjectList onGetProjectList() override;
  virtual void onCloseProject( const uint64_t id ) override;
  virtual TargetList onGetTargetList( const uint64_t id ) override;
  virtual void onRunTarget( const uint64_t id, const std::string& target ) override;
  virtual void onKillTarget( const uint64_t id ) override;
  virtual Status onGetStatus( const uint64_t id ) override;
  virtual void onStartPing( const uint64_t id ) override;
  virtual void onStopPing( const uint64_t id ) override;
  virtual TargetDetails onGetTargetDetails( const uint64_t id, const std::string& target ) override;

private:
  using ProjectItemPtr = std::shared_ptr< Project >;

  std::map< uint64_t, ProjectItemPtr > m_projectsById;
  std::map< std::string, ProjectItemPtr > m_projectsByPath;
  Server m_server;
};
