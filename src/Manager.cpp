#include "Manager.h"
#include "tools.h"
#include "types.h"

#include <iostream>

Manager::Manager()
{
  m_server.setListener( this );
}

Manager::~Manager()
{
}

void Manager::runServer()
{
  try
  {
    m_server.bind();
    m_server.run();
  }
  catch( const std::exception& error )
  {
    std::cerr << "error: " << error.what() << std::endl;
  }
  catch( ... )
  {
    std::cerr << "error: unknown error" << std::endl;
  }
  m_server.close();
}

ProjectInfo Manager::onGetProject( const std::string& path )
{
  if( m_projectsByPath.find( path ) == m_projectsByPath.end() )
  {
    // new project
    auto p = std::make_shared< Project >();
    const auto id = p->open( path );

    // insert
    m_projectsById.insert( { id, p } );
    m_projectsByPath.insert( { path, p } );
  }
  const auto& project = m_projectsByPath.at( path );
  return ProjectInfo{ project->getId(), project->parser().getPath() };
}

ProjectList Manager::onGetProjectList()
{
  ProjectList list;
  for( const auto& [ _, project ] : m_projectsById )
  {
    list.push_back( { project->getId(), project->parser().getPath() } );
  }
  return std::move( list );
}

void Manager::onCloseProject( const uint64_t id )
{
  if( m_projectsById.find( id ) == m_projectsById.end() )
    return;

  const auto p = m_projectsById.at( id );
  const auto path = p->parser().getPath();
  p->close();
  m_projectsById.erase( id );
  m_projectsByPath.erase( path );
}

TargetList Manager::onGetTargetList( const uint64_t id )
{
  if( m_projectsById.find( id ) == m_projectsById.end() )
    throw std::logic_error( tools::format( "project %d no exists", id ) );

  const auto& p = m_projectsById.at( id );
  TargetList list;
  for( const auto& [ name, _ ] : p->parser().getTargets() )
  {
    list.push_back( name );
  }
  return std::move( list );
}

void Manager::onRunTarget( const uint64_t id, const std::string& target )
{
  if( m_projectsById.find( id ) == m_projectsById.end() )
    throw std::logic_error( tools::format( "project %d no exists", id ) );

  auto& p = m_projectsById.at( id );
  p->runTarget( target );
}

void Manager::onKillTarget( const uint64_t id )
{
  if( m_projectsById.find( id ) == m_projectsById.end() )
    throw std::logic_error( tools::format( "project %d no exists", id ) );

  const auto& p = m_projectsById.at( id );
  p->worker().kill();
}

Status Manager::onGetStatus( const uint64_t id )
{
  if( m_projectsById.find( id ) == m_projectsById.end() )
    throw std::logic_error( tools::format( "project %d no exists", id ) );

  const auto& p = m_projectsById.at( id );
  return {};
}

void Manager::onStartPing( const uint64_t id )
{
  if( m_projectsById.find( id ) == m_projectsById.end() )
    throw std::logic_error( tools::format( "project %d no exists", id ) );

  const auto& p = m_projectsById.at( id );
  p->worker().startPing();
}

void Manager::onStopPing( const uint64_t id )
{
  if( m_projectsById.find( id ) == m_projectsById.end() )
    throw std::logic_error( tools::format( "project %d no exists", id ) );

  const auto& p = m_projectsById.at( id );
  p->worker().stopPing();
}

TargetDetails Manager::onGetTargetDetails( const uint64_t id, const std::string& target )
{
  if( m_projectsById.find( id ) == m_projectsById.end() )
    throw std::logic_error( tools::format( "project %d no exists", id ) );

  const auto& project = m_projectsById.at( id );
  const auto& targets = project->parser().getTargets();
  if( targets.find( target ) == targets.end() )
    throw std::logic_error( tools::format( "target %s no exists", target.c_str() ) );

  const auto& tg = targets.at( target );

  TargetDetails details;
  details.cwd = tg.getPath();
  details.commands = tg.getCommands();
  details.lets = tg.getLets();
  details.depends = tg.getDepends();
  return std::move( details );
}

