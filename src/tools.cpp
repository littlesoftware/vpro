#include "tools.h"

#include <cstdarg>
#include <vector>
#include <string>
#include <filesystem>
#include <unistd.h>
#include <sys/file.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <termios.h>

namespace tools
{

std::string format( const char* fmt, ... )
{
  va_list args;
  va_start( args, fmt );

  // get length
  va_list copy;
  va_copy( copy, args );
  const int length = std::vsnprintf( nullptr, 0, fmt, copy );
  va_end( copy );

  std::vector< char > data( length + 1 );
  std::vsnprintf( data.data(), data.size(), fmt, args );
  va_end( args );
  
  return std::string( data.data(), length );
}

std::vector< std::string > split( const std::string& str, char delimiter, bool empty )
{
  std::vector< std::string > result;
  size_t lt = 0;
  size_t sp;
  do
  {
    std::string sub;
    sp = str.find( delimiter, lt ) ;
    if( sp == std::string::npos )
    {
      sub = str.substr( lt );
    }
    else
    {
      sub = str.substr( lt, sp - lt );
      lt = sp + 1;
    }
    if( !sub.empty() || empty )
      result.push_back( std::move( sub ) );
  }
  while( sp != std::string::npos );
  return std::move( result );
}

void createIpcDirectory()
{
  static std::string path = format(
    "%s/vpro-%d",
    std::filesystem::temp_directory_path().c_str(),
    getuid() );
  std::filesystem::create_directory( path );
}

const std::string& getProtocolControl()
{
  static std::string protocol = format(
    "ipc://%s/vpro-%d/control",
    std::filesystem::temp_directory_path().c_str(),
    getuid() );
  return protocol;
}

std::string getProtocolProject( const uint64_t id )
{
  static std::string protocol = format(
    "ipc://%s/vpro-%d/project-%%d",
    std::filesystem::temp_directory_path().c_str(),
    getuid() );
  return format( protocol.c_str(), id );
}

std::string findProject()
{
  return findProject( std::filesystem::current_path() );
}

std::string findProject( const std::string& findPath )
{
  namespace fs = std::filesystem;
  fs::path current_path = findPath;
  fs::path path;
  std::string result;
  for( auto pos : current_path )
  {
    path /= pos;
    fs::path file = path / "vpro.yaml";
    if( fs::is_regular_file( file ) )
    {
      result = file;
    }
  }
  if( result.empty() )
    throw std::runtime_error( "cannot found project" );
  return std::move( result );
}

bool isServerRunned()
{
  bool result = lockServer( true );
  if( result )
    lockServer( false );
  return !result;
}

bool lockServer( const bool lock )
{
  static int file;
  static bool locked = false;
  static std::string path = format(
    "%s/vpro-%d/control.pid",
    std::filesystem::temp_directory_path().c_str(),
    getuid() );
  if( lock )
  {
    if( locked )
      return true;
    file = open( path.c_str(), O_CREAT | O_RDWR, 0666 );
    if( !file )
      return false;
    if( flock( file, LOCK_EX | LOCK_NB ) )
    {
      close( file );
      return false;
    }
    locked = true;
  }
  else
  {
    if( !locked )
      return true;
    flock( file, LOCK_UN );
    close( file );
    locked = false;
  }
  return true;
}

void initKbhit( FILE* file )
{
  int io = fileno( file );
  termios term;
  tcgetattr( io, &term );
  term.c_lflag &= ~ICANON;
  tcsetattr( io, TCSANOW, &term );
}

int kbhit( FILE* file )
{
  int io = fileno( file );
  int bytes;
  ioctl( io, FIONREAD, &bytes );
  return bytes;
}

} // namespace tools
