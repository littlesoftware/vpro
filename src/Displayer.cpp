#include "Displayer.h"
#include "Client.h"
#include "ViewerDebug.h"
#include "ViewerPlain.h"
#include "ViewerJson.h"

#include <iostream>
#include <thread>
#include <dlfcn.h>

Displayer::Displayer()
{
  setPlainFormat();
  m_subscriber.setListener( this );
  m_client.connect();
}

Displayer::~Displayer()
{
  m_subscriber.close();
  m_client.close();
}

bool Displayer::setProject( const std::string& path )
{
  m_id = m_client.getProject( path );
  m_subscriber.connect( m_id );
  return true;
}

void Displayer::printTargets()
{
  m_viewer->onTargetList( m_id, m_client.getTargetList( m_id ) );
}

void Displayer::printTargetDetails( const std::string& target )
{
  m_viewer->onTargetDetails( m_id, target, m_client.getTargetDetails( m_id, target ) );
}

bool Displayer::run( const std::string& target )
{
  m_target = target;
  m_viewerPingSuccess = false;
  std::thread th( &Displayer::viewer, this );
  m_client.startPing( m_id );
  while( !m_viewerPingSuccess )
    std::this_thread::yield();
  m_client.stopPing( m_id );
  m_client.runTarget( m_id, target );
  th.join();
  return true;
}

void Displayer::setPlainFormat()
{
  m_viewer = std::make_shared< ViewerPlain >();
}

void Displayer::setDebugFormat()
{
  m_viewer = std::make_shared< ViewerDebug >();
}

void Displayer::setJsonFormat()
{
  m_viewer = std::make_shared< ViewerJson >();
}

void Displayer::onStartTarget( const std::string& name )
{
  m_currentTarget = m_target == name;
  m_viewer->onStartTarget( name );
}

void Displayer::onFinishTarget( const int32_t code )
{
  if( m_currentTarget || code != 0 )
    m_subscriber.stop();
  m_viewer->onFinishTarget( code );
}

void Displayer::onOutput( const std::string& message )
{
  m_viewer->onOutput( message );
}

void Displayer::onOutputError( const std::string& message )
{
  m_viewer->onOutputError( message );
}

void Displayer::onCloseProject()
{
  m_viewer->onCloseProject();
}

void Displayer::onPing()
{
  m_viewerPingSuccess = true;
  m_viewer->onPing();
}

void Displayer::viewer()
{
  m_subscriber.run();
}
