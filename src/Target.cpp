#include <filesystem>
#include <iostream>
#include <thread>
#include <chrono>
#include <cpp-subprocess/subprocess.hpp>
#include "Target.h"
#include "tools.h"

Target::Target()
{
}

Target::~Target()
{
}

void Target::setName( const std::string& name )
{
  m_name = name;
}

const std::string& Target::getName() const
{
  return m_name;
}

void Target::addDepend( const std::string& name )
{
  m_depends.insert( name );
}

const Depends& Target::getDepends() const
{
  return m_depends;
}

void Target::setPath( const std::string& path )
{
  m_path = path;
}

const std::string& Target::getPath() const
{
  return m_path;
}

void Target::addCommand( const Command& command )
{
  if( !command.empty() )
    m_commands.push_back( command );
}

const Commands& Target::getCommands() const
{
  return m_commands;
}

void Target::setEnvironment( const std::string& name, const std::string& value )
{
  m_environments.insert( { name, value } );
}

const Environments& Target::getEnvironments() const
{
  return m_environments;
}

void Target::setLet( const std::string& name, const std::string& value )
{
  m_lets.insert( { name, value } );
}

const Lets& Target::getLets() const
{
  return m_lets;
}
