#pragma once

#include "types.h"

#include <string>
#include <cstdint>
#include <list>
#include <optional>

class ServerListener
{
public:
  virtual ~ServerListener() = default;

  virtual ProjectInfo onGetProject( const std::string& path ) = 0;
  virtual ProjectList onGetProjectList() = 0;
  virtual void onCloseProject( const uint64_t id ) = 0;
  virtual TargetList onGetTargetList( const uint64_t id ) = 0;
  virtual void onRunTarget( const uint64_t id, const std::string& target ) = 0;
  virtual void onKillTarget( const uint64_t id ) = 0;
  virtual Status onGetStatus( const uint64_t id ) = 0;
  virtual void onStartPing( const uint64_t id ) = 0;
  virtual void onStopPing( const uint64_t id ) = 0;
  virtual TargetDetails onGetTargetDetails( const uint64_t id, const std::string& target ) = 0;
};

