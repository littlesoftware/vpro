# vpro - vim project

## About

VPRO - instraction of execution targets as makefile. But use special format based
on yaml format. The main goal is find project file in parent directory and perform build and
execution from everywhere.

### Targets

* [X] Find project path
* [X] Simple parse YAML file
* [X] Print targets in project
* [X] Simple execute target
* [X] Add depends
* [X] Add custom directory of execute target
* [X] Add interrupt handler <Ctrl-c> for success result
* [X] Add print all target depends for execute certain target
* [X] Execute only target (without depends)
* [X] Add skip option (test of skip for depends)

## Build and install

```bash
git clone https://gitlab.com/littlesoftware/vpro.git
cd vpro
mkdir build
cd build
conan install ..
cmake ..
cmake --build .
sudo cmake --build . --target install
```
or with vpro
```bash
git clone https://gitlab.com/littlesoftware/vpro.git
cd vpro
vpro install
```

## License

License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribate it.
There is NO WARRANTY, to the extent permitted by law.

Written by Nikolaev Anton.

