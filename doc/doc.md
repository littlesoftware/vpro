# Messages of client/server/viewer

![msg_ctrl](http://www.plantuml.com/plantuml/proxy?cache=no&src=https://gitlab.com/littlesoftware/vpro/-/raw/main/doc/msg_ctrl.puml?ref_type=heads)

# Structure of message client->server

| Type    | Structure                                        |
| ---     | ---                                              |
|         | (?e)[error message]                              |
| get     | (g)[path to vpro file project]                   |
|         | (go)[number]                                     |
|         | (ge)not found project                            |
|         | (ge)parse error                                  |
| close   | (c)[number of socket]                            |
|         | (co)                                             |
|         | (ce)not found                                    |
| list    | (l)                                              |
|         | (lo)[number of socket][ ][path to project][\0]   |
| details | (d)[number of socket]                            |
|         | (do)[target1:target2]                            |
| run     | (r)[number of socket][ ][target]                 |
|         | (ro)                                             |


